# Basic wordpress site using docker and NGINX

This repository is used to create a clean wordpress site using docker, we use nginx instead of the default apache one that comes with the docker image.
The nginx configuration is basic